async function main() {
  const response = await fetch('target/wasm32-unknown-unknown/release/callbacktest.wasm');
  const buffer = await response.arrayBuffer();

  const module = await WebAssembly.compile(buffer);

  let instance = 0;
  let callback = 0;
  let userdata = 0;
  let cleanup = 0;

  var imports = {
    env: {
      wasm_console_log(pointer, len) {
        const memory = instance.exports.memory;
        const array = new Uint8Array(memory.buffer, pointer, len);
        const decoder = new TextDecoder();
        console.log(decoder.decode(array));
      },
      wasm_set_callback(cb, ud, cl) {
        const prev_userdata = userdata;
        const prev_cleanup = cleanup;
        callback = cb;
        userdata = ud;
        cleanup = cl;
        if (prev_userdata != 0) {
          const tbl = instance.exports.__indirect_function_table;
          const func = tbl.get(prev_cleanup);
          func(prev_userdata);
        }
      },
      wasm_call_callback() {
        const tbl = instance.exports.__indirect_function_table;
        const func = tbl.get(callback);
        func(userdata);
      },
    },
  };

  instance = await WebAssembly.instantiate(module, imports);
  console.log("pre-main");
  instance.exports.main();
  if (userdata != 0) {
    const tbl = instance.exports.__indirect_function_table;
    const func = tbl.get(cleanup);
    func(userdata);
  }
  console.log("post-main");
}
await main();
