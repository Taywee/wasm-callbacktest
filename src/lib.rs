mod ffi {
    use std::ffi::c_void;
    extern "C" {
        pub fn wasm_console_log(string: *const u8, len: usize);
        pub fn wasm_set_callback(
            cb: extern "C" fn(userdata: *mut c_void),
            userdata: *mut c_void,
            cleanup: extern "C" fn(userdata: *mut c_void),
        );
        pub fn wasm_call_callback();
    }
}

mod lib {
    use std::{ffi::c_void, ptr};
    pub fn log<S>(message: S)
    where
        S: AsRef<str>,
    {
        let bytes = message.as_ref().as_bytes();
        unsafe {
            crate::ffi::wasm_console_log(bytes.as_ptr(), bytes.len());
        }
    }

    extern "C" fn trampoline<F>(userdata: *mut c_void)
    where
        F: FnMut(),
    {
        let func = unsafe { &mut *(userdata as *mut F) };
        func();
    }

    extern "C" fn cleanup<F>(userdata: *mut c_void)
    where
        F: FnMut(),
    {
        unsafe {
            ptr::drop_in_place(userdata as *mut F);
        }
    }

    pub fn set_callback<F>(function: F)
    where
        F: FnMut(),
    {
        let boxed_function: Box<F> = Box::new(function);
        unsafe {
            crate::ffi::wasm_set_callback(
                trampoline::<F>,
                Box::into_raw(boxed_function) as *mut c_void,
                cleanup::<F>,
            );
        }
    }

    pub fn call_callback() {
        unsafe {
            crate::ffi::wasm_call_callback();
        }
    }
}

#[derive(Default, Debug)]
struct Counter(u32);

impl Counter {
    pub fn count(&mut self) -> u32 {
        self.0 += 1;
        self.0
    }
}

impl Drop for Counter {
    fn drop(&mut self) {
        let count = self.0;
        lib::log(format!("Dropping counter with {count}"));
    }
}

#[no_mangle]
pub extern "C" fn main() {
    lib::log("startup");
    lib::set_callback(|| lib::log("Inside callback"));
    lib::log("calling");
    lib::call_callback();
    lib::log("calling");
    lib::call_callback();
    lib::log("changing");
    lib::set_callback(|| lib::log("Inside callback 2"));
    lib::log("calling");
    lib::call_callback();
    lib::log("calling");
    lib::call_callback();
    let mut counter = Counter::default();
    lib::set_callback(move || {
        let count = counter.count();
        lib::log(format!("Counter is now {count}"));
    });
    lib::log("calling");
    lib::call_callback();
    lib::log("calling");
    lib::call_callback();
    lib::log("calling");
    lib::call_callback();
    lib::log("calling");
    lib::call_callback();
    let mut counter = Counter::default();
    lib::set_callback(move || {
        let count = counter.count();
        lib::log(format!("Counter is now {count}"));
    });
    lib::log("shutdown");
}
